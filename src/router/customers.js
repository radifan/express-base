import { Router } from 'express';
import CustomerService from '../services/CustomerService';

const router = Router()
  .get('/', CustomerService.all)
  .get('/:userId', CustomerService.findById);

export default {
  endpoint: 'customers',
  router,
};
