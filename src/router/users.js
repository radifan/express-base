import { Router } from 'express';
import UserService from '../services/UserService';

const router = Router()
  .get('/', UserService.all)
  .get('/:userId', UserService.findById);

export default {
  endpoint: 'users',
  router,
};
