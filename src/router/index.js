import users from './users';
import customers from './customers';

export default [
  users,
  customers,
];
