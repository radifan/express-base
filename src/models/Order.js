import { Entity, Column, ManyToOne } from 'typeorm';
import BaseModel from './BaseModel';
import Customer from './Customer';

@Entity('sdq_orders')
export default class Order extends BaseModel {
  @Column({ name: 'code', type: 'varchar' })
  code = '';

  @ManyToOne(type => Customer, customer => customer.orders, { cascadeInsert: true }) // eslint-disable-line
  customer = '';

  @Column({ name: 'tracking_number', type: 'varchar', nullable: true })
  trackingNumber = '';

  @Column({ name: 'status', type: 'varchar', nullable: true })
  status = '';

  @Column({ name: 'date', type: 'date', nullable: true })
  date = '';

  @Column({
    name: 'total_sale_price',
    type: 'numeric',
    scale: 2,
    precision: 19,
    nullable: true,
  })
  totalSalePrice = 0;

  @Column({
    name: 'total_base_price',
    type: 'numeric',
    scale: 2,
    precision: 19,
    nullable: true,
  })
  totalBasePrice = 0;

  @Column({ name: 'products', type: 'jsonb', nullable: true })
  products = '';

  @Column({ name: 'history', type: 'jsonb', nullable: true })
  history = '';
}
