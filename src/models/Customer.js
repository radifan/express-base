import { Entity, Column, OneToMany } from 'typeorm';
import BaseModel from './BaseModel';
import CustomerAddress from './CustomerAddress';
import Order from './Order';

@Entity('sdq_customers')
export default class User extends BaseModel {
  @Column({ name: 'full_name', type: 'varchar' })
  fullName = '';

  @Column({ name: 'email', type: 'varchar', nullable: true })
  email = '';

  @Column({ name: 'phone_number', type: 'varchar' })
  phoneNumber = '';

  @Column({ name: 'password', type: 'varchar', nullable: true })
  password = '';

  @Column({ name: 'identity_number', type: 'varchar', nullable: true })
  identityNumber = '';

  @OneToMany(type => CustomerAddress, address => address.customer, { cascadeInsert: true, nullable: true }) // eslint-disable-line
  address = [];

  @OneToMany(type => Order, orders => orders.customer, { cascadeInsert: true, nullable: true }) // eslint-disable-line
  orders = [];
}
