import { BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn } from 'typeorm';

export default class BaseModel extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  id = 0;

  @CreateDateColumn({ name: 'created_at', nullable: true })
  createdAt = '';

  @Column({ name: 'updated_at', type: 'date', nullable: true })
  updatedAt = '';

  @Column({ name: 'is_active', type: 'boolean' })
  isActive = true;
}
