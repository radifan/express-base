import { Entity, Column, ManyToOne } from 'typeorm';
import BaseModel from './BaseModel';
import Customer from './Customer';

@Entity('sdq_customer_address')
export default class CustomerAddress extends BaseModel {
  @Column({ name: 'name', type: 'varchar' })
  name = '';

  @ManyToOne(type => Customer, customer => customer.address, { cascadeInsert: true }) // eslint-disable-line
  customer = '';

  @Column({ name: 'full_address', type: 'varchar' })
  fullAddress = '';

  @Column({ name: 'postal_code', type: 'varchar', nullable: true })
  postalCode = '';

  @Column({ name: 'receiver_name', type: 'varchar' })
  receiverName = '';

  @Column({ name: 'level_0', type: 'varchar', nullable: true })
  l0 = '';

  @Column({ name: 'level_1', type: 'varchar', nullable: true })
  l1 = '';

  @Column({ name: 'level_2', type: 'varchar', nullable: true })
  l2 = '';

  @Column({ name: 'level_3', type: 'varchar', nullable: true })
  l3 = '';

  @Column({ name: 'level_4', type: 'varchar', nullable: true })
  l4 = '';

  @Column({ name: 'level_5', type: 'varchar', nullable: true })
  l5 = '';
}
