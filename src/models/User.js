import { Entity, Column } from 'typeorm';
import BaseModel from './BaseModel';

@Entity('core_user')
export default class User extends BaseModel {
  @Column({ name: 'full_name', type: 'varchar' })
  fullName = '';

  @Column({ name: 'email', type: 'varchar', nullable: true })
  email = '';

  @Column({ name: 'phone_number', type: 'varchar' })
  phoneNumber = '';

  @Column({ name: 'password', type: 'varchar' })
  password = '';

  @Column({ name: 'identity_number', type: 'varchar', nullable: true })
  identityNumber = '';

  @Column({ name: 'address', type: 'varchar' })
  address = '';
}
