import { Entity, Column } from 'typeorm';
import BaseModel from './BaseModel';

@Entity('sdq_products')
export default class Product extends BaseModel {
  @Column({ name: 'name', type: 'varchar' })
  name = '';

  @Column({ name: 'code', type: 'varchar' })
  code = '';

  @Column({ name: 'description', type: 'varchar', nullable: true })
  description = '';

  @Column({
    name: 'base_price', type: 'numeric', scale: 2, precision: 19,
  })
  basePrice = 0;

  @Column({
    name: 'sale_price', type: 'numeric', scale: 2, precision: 19,
  })
  salePrice = 0;

  @Column({ name: 'stock', type: 'numeric', scale: 5 })
  stock = '';
}
