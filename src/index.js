import 'reflect-metadata';
import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import { createConnection } from 'typeorm';

import dbConfig from './config/db';
import routes from './router';

createConnection(dbConfig).then(() => {
  const app = express();
  app.disable('x-powered-by');

  app.use(logger('dev', {
    skip: () => app.get('env') === 'test',
  }));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  // Routes
  routes.forEach(({ endpoint, router }) => {
    const uri = `/api/v1/${endpoint}`;
    app.use(uri, router);
  });

  // Catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    res.status(404).json({
      message: 'page not found',
    });
    next(err);
  });

  // Error handler
  app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
    res
      .status(err.status || 500);
  });

  const { PORT = 8080 } = process.env;
  app.listen(PORT, () => console.log(`Listening on port ${PORT}`)); // eslint-disable-line no-console
}).catch((error) => {
  // TODO: handle error
  console.log(error);
});
