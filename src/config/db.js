import path from 'path';

const dbConfig = {
  type: process.env.DB_CLIENT,
  database: process.env.DB_NAME,
  schema: process.env.DB_SCHEMA,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  entities: [
    path.join(__dirname, '../models/*.js'),
  ],
  synchronize: true,
};

export default dbConfig;
