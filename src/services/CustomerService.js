import Customer from '../models/Customer';

export const Controller = {
  all: async (req, res) => {
    res.json(await Customer.find({ relations: ['address', 'orders'] }));
  },

  findById: async (req, res) => {
    res.json(await Customer.findOneById(req.params.userId));
  },
};

export default Controller;
