import User from '../models/User';

export const Controller = {
  all: async (req, res) => {
    res.json(await User.find());
  },

  findById: async (req, res) => {
    res.json(await User.findOneById(req.params.userId));
  },
};

export default Controller;
